###############################
##                           ##
##  GUI for Test Sensor API  ##
##                           ##
##  Gengdai Liu, 2012-05-25  ##
##                           ##
###############################


from fltk import *
import math
import os
import sys

# add 'gen-py' to system path
thrift_module_path = os.getcwd() + '\\gen-py'
sys.path.append(thrift_module_path)
# import services
from Inputs import SoundService
from Inputs import TrackingService
from Inputs import FaceService
from Inputs import UserTrackingService
from Inputs import SpeechRecognitionService
from Inputs import TouchService
from Inputs.ttypes import *

# import Thrift
from thrift.transport import TTransport
from thrift.transport import TSocket
from thrift.protocol import TBinaryProtocol
from thrift.protocol import TCompactProtocol

# result = u.v
def dot_product(u,v):
    result = u.x * v.x + u.y * v.y + u.z * v.z
    return result

# result = scalar * v
def scale_vec(scalar,v):
    result = I2P.ttypes.Vec3()
    result.x = scalar * v.x
    result.y = scalar * v.y
    result.z = scalar * v.z
    return result

# result = u + v
def add_vec(u,v):
    result = I2P.ttypes.Vec3(u.x+v.x, u.y+v.y, u.z+v.z)
    return result

# result = u x v
def cross_product(u,v):
    result = I2P.ttypes.Vec3()
    result.x = u.y * v.z - u.z * v.y
    result.y = u.z * v.x - u.x * v.z
    result.z = u.x * v.y - u.y * v.x
    return result

# get quaternion from axis and angle
def quaternion_from_axis_angle(angle,axis):
    w = math.cos(angle/2)
    x = axis.x * math.sin(angle/2)
    y = axis.y * math.sin(angle/2)
    z = axis.z * math.sin(angle/2)
    q = I2P.ttypes.Vec4(x,y,z,w)
    return q

# result = q^-1
def inverse_quaternion(q):    
    len_square = q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w
    result = I2P.ttypes.Vec4(-q.x/len_square,-q.y/len_square,-q.z/len_square,q.w/len_square)
    return result

# result = q1*q2
def quaternion_multiply(q1,q2):
    q = I2P.ttypes.Vec4()
    v1 = I2P.ttypes.Vec3(q1.x,q1.y,q1.z)
    v2 = I2P.ttypes.Vec3(q2.x,q2.y,q2.z)
    q.w = q1.w * q2.w - dot_product(v1,v2)
    v3 = add_vec(add_vec(scale_vec(q1.w,v2),scale_vec(q2.w,v1)),cross_product(v1,v2))
    q.x = v3.x
    q.y = v3.y
    q.z = v3.z
    return q

#####################################################
# convert an euler angle to quaternion	            #
# we assume the order of rotation is x -> y -> z    #
#####################################################
def euler_to_quaternion(euler):
    axis_x = I2P.ttypes.Vec3(1.0,0.0,0.0)
    axis_y = I2P.ttypes.Vec3(0.0,1.0,0.0)
    axis_z = I2P.ttypes.Vec3(0.0,0.0,1.0)
    q1 = quaternion_from_axis_angle(euler.x,axis_x)
    q2 = quaternion_from_axis_angle(euler.y,axis_y)
    q3 = quaternion_from_axis_angle(euler.z,axis_z)
    result = quaternion_multiply(quaternion_multiply(q1,q2),q3)
    return result


class RobotInf:
    def __init__(self,pos,rot,head_rot):
        self.position = pos
        self.body_orientation = rot
        self.head_orientation = head_rot
        #self.forward = I2P.ttypes.Vec3(1.0,0.0,0.0)
    def moved(self,delta):
        self.position = add_vec(self.position,self.new_move(delta,self.body_orientation))
    def body_rotated(self,delta):
        self.body_orientation = quaternion_multiply(self.body_orientation,delta)
        
    def head_rotated(self,delta):
         self.head_orientation = quaternion_multiply(self.head_orientation,delta)
         
    def new_move(self,move,q):
        q_move = I2P.ttypes.Vec4(move.x, move.y, move.z, 0.0)
        q_new_move =quaternion_multiply(q,quaternion_multiply(q_move,inverse_quaternion(q)))
        result = I2P.ttypes.Vec3(q_new_move.x,q_new_move.y,q_new_move.z)
        return result

# main window
class MainWindow(Fl_Window):
    def __init__(self, xpos, ypos, width, height, label=None):
        Fl_Window.__init__(self, xpos, ypos, width, height, label)
        self.tab = Fl_Tabs(10,10, 300, 480)
        self.tab.pyChildren = {}
        self.createTab_1()
        self.createTab_2()
        self.createTab_3()
        self.createTab_4()
        self.createTab_5()
        self.createTab_6()
        self.tab.end()
        self.initRobotInf()

    def initRobotInf(self):
        self.robotInf = RobotInf(I2P.ttypes.Vec3(0.0,0.0,0.0),I2P.ttypes.Vec4(0.0,0.0,0.0,1.0),I2P.ttypes.Vec4(0.0,0.0,0.0,1.0))

    # the 1st tab: SOUND
    def createTab_1(self):
        group_1 = Fl_Group(10, 30, 300, 480, "Sound")
        group_1.pyChildren ={}
        
        # buttons for connection and disconnection
        connect_button_1 = Fl_Button(60, 50, 80, 30, "Connect")
        connect_button_1.callback(self.connect_sound)
        group_1.pyChildren['connect_button'] = connect_button_1
        disconnect_button_1 = Fl_Button(160, 50, 80, 30, "Disconnect")
        disconnect_button_1.callback(self.disconnect_sound)
        disconnect_button_1.deactivate()
        group_1.pyChildren['disconnect_button'] = disconnect_button_1
        
        # sensor ID string input
        sensorID = Fl_Input(100,100,100,30,"SensorID")
        group_1.pyChildren['sensorID'] = sensorID

        # timestamp value input
        timestamp = Fl_Value_Input( 100, 140, 100, 30, "TimeStamp")
        timestamp.minimum(0.0)
        timestamp.maximum(100.0)
        group_1.pyChildren['timestamp'] = timestamp

        # a pack containing some sliders for AudioLocalization
        self.pack_sound_localization = Fl_Pack( 20 , 200, 250, 200)
        self.pack_sound_localization.type(FL_VERTICAL)
        self.pack_sound_localization.spacing(30)
        self.pack_sound_localization.children = {}
        azimuth_slider = Fl_Hor_Value_Slider( 10, 10, 10, 25, "Azimuth")
        azimuth_slider.minimum(0.0)
        azimuth_slider.maximum(360.0)
        self.pack_sound_localization.children['azimuth'] = azimuth_slider
        alevation_slider = Fl_Hor_Value_Slider( 10, 10, 10, 25, "Alevation (available only for NAO)")
        alevation_slider.minimum(0.0)
        alevation_slider.maximum(360.0)
        self.pack_sound_localization.children['elevation'] = alevation_slider
        intensity_slider = Fl_Hor_Value_Slider( 10, 10, 10, 25, "Intesity")
        intensity_slider.minimum(0.0)
        intensity_slider.maximum(1.0)
        self.pack_sound_localization.children['intensity'] = intensity_slider
        confidence_slider = Fl_Hor_Value_Slider( 10, 10, 10, 25, "Confidence")
        confidence_slider.minimum(0.0)
        confidence_slider.maximum(1.0)
        self.pack_sound_localization.children['confidence'] = confidence_slider        
        self.pack_sound_localization.end()
        group_1.pyChildren['sound_localization_pack']=self.pack_sound_localization
        
        # buttons for sound test
        sound_start_button = Fl_Button(20,430,80,30,'Start')
        sound_start_button.name = 'start'
        sound_start_button.callback(self.sound_test)        
        group_1.pyChildren['sound_start_button'] = sound_start_button

        sound_stop_button = Fl_Button(120,430,80,30,'Stop')
        sound_stop_button.name = 'stop'
        sound_stop_button.callback(self.sound_test)
        group_1.pyChildren['sound_stop_button'] = sound_stop_button

        sound_move_button = Fl_Button(220,430,80,30,'Move')
        sound_move_button.name = 'move'
        sound_move_button.callback(self.sound_test)
        group_1.pyChildren['sound_move_button'] = sound_move_button

        group_1.end()
        self.tab.pyChildren['sound'] = group_1
        self.sound_connected = False
    
    # the 2nd tab: Track (robot)
    def createTab_2(self):
        group_2 = Fl_Group(10, 30, 300, 480, "Track")
        group_2.pyChildren ={}

        # buttons for connection and disconnection
        connect_button_2 = Fl_Button(60, 50, 80, 30, "Connect")
        connect_button_2.callback(self.connect_tracking)
        group_2.pyChildren['connect_button'] = connect_button_2
        disconnect_button_2 = Fl_Button(160, 50, 80, 30, "Disconnect")
        disconnect_button_2.callback(self.disconnect_tracking)
        disconnect_button_2.deactivate()
        group_2.pyChildren['disconnect_button'] = disconnect_button_2

        # sensor ID string input
        sensorID = Fl_Input(100,100,100,30,"SensorID")
        group_2.pyChildren['sensorID'] = sensorID

        # timestamp value input
        timestamp = Fl_Value_Input( 100, 140, 100, 30, "TimeStamp")
        timestamp.minimum(0.0)
        timestamp.maximum(100.0)
        group_2.pyChildren['timestamp'] = timestamp

        # a pack containing a choice menu for object name selection
        self.object_selection_pack = Fl_Pack( 20 , 175, 250, 100)
        self.object_selection_pack.children = {}
        object_name_box = Fl_Box(10,5,50,30,'Please select one of the object names:')
        object_name_box.box(FL_NO_BOX)
        object_name_choice = Fl_Choice(10, 60, 40,30, None)
        pulldown= (  ("Head",	FL_ALT+ord('h')),
	     ("Robot",	FL_ALT+ord('r')),
	     (None,))
        object_name_choice.copy(pulldown)
        object_name_choice.callback(self.choiceChanged)
        self.object_selection_pack.children['choice'] = object_name_choice
        self.object_selection_pack.end()
        group_2.pyChildren['object_selection_pack'] = self.object_selection_pack
                
        self.robot_position_change = []
        position_change_box = Fl_Box(95,240,50,30,'Please input position changed:') 
        position_change_box.box(FL_NO_BOX)
        position_change_counter_x = Fl_Simple_Counter(20,270,80,30, 'X')
        self.robot_position_change.append(position_change_counter_x)
        position_change_counter_y = Fl_Simple_Counter(120,270,80,30, 'Y')
        self.robot_position_change.append(position_change_counter_y)
        position_change_counter_z = Fl_Simple_Counter(220,270,80,30, 'Z')
        self.robot_position_change.append(position_change_counter_z)
        self.position_reset_button = Fl_Button(130,320,60,30, 'reset')
        self.position_reset_button.name = 'position'
        self.position_reset_button.callback(self.robot_reset)
        for counter in self.robot_position_change:
                counter.deactivate()
        self.position_reset_button.deactivate()
                
        self.robot_orientation_change = []
        orientation_change_box = Fl_Box(95,350,50,30,'Please input orientation changed:')
        orientation_change_box.box(FL_NO_BOX)
        orientation_change_dial_roll  = Fl_Line_Dial(20,380,45,45,'Roll')        
        orientation_change_dial_pitch = Fl_Line_Dial(95,380,45,45,'Pitch')        
        orientation_change_dial_yaw = Fl_Line_Dial(165,380,45,45,'Yaw')        
        self.robot_orientation_change.append(orientation_change_dial_roll)
        self.robot_orientation_change.append(orientation_change_dial_pitch)
        self.robot_orientation_change.append(orientation_change_dial_yaw)
        for dial in self.robot_orientation_change:
            dial.angle1(90)
            dial.value(0.5)
            dial.angle2(270)
            dial.value(0.5)
        self.orientation_reset_button = Fl_Button(230, 390, 60, 30, 'reset')
        self.orientation_reset_button.name = 'orientation'
        self.orientation_reset_button.callback(self.robot_reset)

        robot_move_button = Fl_Button(120,450,80,30,'Move')
        robot_move_button.callback(self.tracking_test)
        group_2.pyChildren['robot_move_button'] = robot_move_button

        group_2.end()
        self.tab.pyChildren['track'] = group_2
        self.track_connected = False

    def choiceChanged(self,menu):
        #print menu.value()
        index = menu.value()
        if index == 0: #position change should be inactive
            for counter in self.robot_position_change:
                counter.deactivate()
            self.position_reset_button.deactivate()
        else:
            for counter in self.robot_position_change:
                counter.activate()
            self.position_reset_button.activate()

    # reset position and orientation
    def robot_reset(self,button):
        name = button.name
        if name == 'position':
            for counter in self.robot_position_change:
                counter.value(0)
        else:
            for dial in self.robot_orientation_change:
                dial.angle1(90)
                dial.value(0.5)
                dial.angle2(270)
                dial.value(0.5)

    # the 3rd tab: Face Recoginition
    def createTab_3(self):
        group_3 = Fl_Group(10, 30, 300, 480, "Face")
        group_3.pyChildren ={}
        connect_button_3 = Fl_Button(60, 50, 80, 30, "Connect")
        connect_button_3.callback(self.connect_face)
        group_3.pyChildren['connect_button'] = connect_button_3
        disconnect_button_3 = Fl_Button(160, 50, 80, 30, "Disconnect")
        disconnect_button_3.callback(self.disconnect_face)
        group_3.pyChildren['disconnect_button'] = disconnect_button_3

        group_3.end()
        self.tab.pyChildren['face'] = group_3

    # the 4th tab: User Tracking
    def createTab_4(self):
        group_4 = Fl_Group(10, 30, 300, 480, "User")
        group_4.pyChildren ={}
        connect_button_4 = Fl_Button(60, 50, 80, 30, "Connect")
        connect_button_4.callback(self.connect_usertracking)
        group_4.pyChildren['connect_button'] = connect_button_4
        disconnect_button_4 = Fl_Button(160, 50, 80, 30, "Disconnect")
        disconnect_button_4.callback(self.disconnect_usertracking)
        group_4.pyChildren['disconnect_button'] = connect_button_4

        group_4.end()
        self.tab.pyChildren['user'] = group_4

    # the 5th tab: Speech Recognition
    def createTab_5(self):
        group_5 = Fl_Group(10, 30, 300, 480, "Speech")
        group_5.pyChildren ={}
        connect_button_5 = Fl_Button(60, 50, 80, 30, "Connect")
        connect_button_5.callback(self.connect_speech)
        group_5.pyChildren['connect_button'] = connect_button_5
        disconnect_button_5 = Fl_Button(160, 50, 80, 30, "Disconnect")
        disconnect_button_5.callback(self.disconnect_speech)
        group_5.pyChildren['disconnect_button'] = disconnect_button_5

        group_5.end()
        self.tab.pyChildren['speech'] = group_5

    # the 6th tab: Touch
    def createTab_6(self):
        group_6 = Fl_Group(10, 30, 300, 480, "Touch")
        group_6.pyChildren ={}
        connect_button_6 = Fl_Button(60, 50, 80, 30, "Connect")
        connect_button_6.callback(self.connect_touch)
        group_6.pyChildren['connect_button'] = connect_button_6
        disconnect_button_6 = Fl_Button(160, 50, 80, 30, "Disconnect")
        disconnect_button_6.callback(self.disconnect_touch)
        group_6.pyChildren['disconnect_button'] = disconnect_button_6

        group_6.end()
        self.tab.pyChildren['touch'] = group_6
    
    # connect to sound service
    def connect_sound(self,event):
        print 'connecting to sound service'
        try:
            socket = TSocket.TSocket("localhost", 9090)
            self.sound_transport = TTransport.TFramedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocolFactory().getProtocol(self.sound_transport)
            self.sound_transport.open()
            self.sound_service = SoundService.Client(protocol)
        except TSocket.TTransportException:
            print 'can not connect to sound service...:('
            return

        # if successful, set this button inactive
        print 'sound server connected'
        group_1 = self.tab.pyChildren['sound']
        connect_button = group_1.pyChildren['connect_button']
        disconnect_button = group_1.pyChildren['disconnect_button']
        connect_button.deactivate()
        disconnect_button.activate()
        self.sound_connected = True

    # disconnect from sound service
    def disconnect_sound(self,event):
        print 'disconnecting from sound service'
        # clean up
        self.sound_transport.close()
        
        # if successful, set this button inactive
        print 'sound server disconnected'
        group_1 = self.tab.pyChildren['sound']
        disconnect_button = group_1.pyChildren['disconnect_button']
        connect_button =  group_1.pyChildren['connect_button']
        connect_button.activate()
        disconnect_button.deactivate()
        self.sound_connected = False

    def connect_tracking(self,event):
        print 'connecting to robot tracking service'
        try:
            socket = TSocket.TSocket("localhost", 9091)
            self.tracking_transport = TTransport.TFramedTransport(socket)
            protocol = TBinaryProtocol.TBinaryProtocolFactory().getProtocol(self.tracking_transport)
            self.tracking_transport.open()
            self.tracking_service = TrackingService.Client(protocol)
        except TSocket.TTransportException:
            print 'can not connect to tracking service...:('
            return
          
        # if successful, set this button inactive
        print 'tracking server connected'
        group_2 = self.tab.pyChildren['track']
        connect_button = group_2.pyChildren['connect_button']
        disconnect_button = group_2.pyChildren['disconnect_button']
        disconnect_button.activate()
        connect_button.deactivate()
        self.track_connected = True

    def disconnect_tracking(self,event):
        print 'disconnecting from robot tracking service'
        # clean up
        self.tracking_transport.close()

        # if successful, set this button inactive
        print 'tracking server disconnected'
        group_2 = self.tab.pyChildren['track']
        connect_button = group_2.pyChildren['connect_button']
        disconnect_button = group_2.pyChildren['disconnect_button']
        disconnect_button.deactivate()
        connect_button.activate()
        self.track_connected = False

    #####################################
    ###### NOT IN USE IN VERSION_1 ######
    #####################################
    def connect_face(self,event):
        print 'connecting to face recognition service'
    def disconnect_face(self,event):
        print 'disconnecting from face recognition service'
    def connect_usertracking(self,event):
        print 'connecting to user tracking service'
    def disconnect_usertracking(self,event):
        print 'disconnecting from user tracking service'
    def connect_speech(self,event):
        print 'connecting to speech recognition service'
    def disconnect_speech(self,event):
        print 'disconnecting from speech recognition service'
    def connect_touch(self,event):
        print 'connecting to touch service'
    def disconnect_touch(self,event):
        print 'disconnecting from touch service'

    def sound_test(self,widget):
        if self.sound_connected == False:
            fl_alert("Please connect to sound service first")
            return
        group_1 = self.tab.pyChildren['sound']
        sensorID_input = group_1.pyChildren['sensorID']
        timestamp_input = group_1.pyChildren['timestamp']
        audio_pack = self.pack_sound_localization
        azimuth_slider = audio_pack.children['azimuth']
        azimuth = azimuth_slider.value()
        elevation_slider = audio_pack.children['elevation']
        elevation = elevation_slider.value()
        intensity_slider = audio_pack.children['intensity']
        intensity = intensity_slider.value()
        confidence_slider = audio_pack.children['confidence']
        confidence = confidence_slider.value()
        source = AudioLocalization(azimuth,elevation,intensity,confidence)
        
        sensorID = sensorID_input.value()
        timestamp = long(timestamp_input.value())
        
        if widget.name == 'start':
            self.sound_service.soundStart(sensorID,timestamp,source)
            
        elif widget.name == 'stop':
            self.sound_service.soundStop(sensorID,timestamp,source)
        else:
            self.sound_service.soundSourceMoved(sensorID,timestamp,source)

    def tracking_test(self, widget):
        if not self.track_connected:
            fl_alert("Please connect to tracking service first")
            return
        group_2 = self.tab.pyChildren['track']
        sensorID_input = group_2.pyChildren['sensorID']
        timestamp_input = group_2.pyChildren['timestamp']
        sensorID = sensorID_input.value()
        timestamp = long(timestamp_input.value())
        object_name_choice = self.object_selection_pack.children['choice']
        if object_name_choice.value()==0:
            object_name = 'nao_head'
        else:
            object_name = 'nao'
        #if self.robot_position_change[0].active() == 0:
        #    delta_x = delta_y = delta_z = 0
        delta_x = self.robot_position_change[0].value()
        delta_y = self.robot_position_change[1].value()
        delta_z = self.robot_position_change[2].value()
        position_changed = I2P.ttypes.Vec3(delta_x, delta_y, delta_z)
        delta_roll = -(self.robot_orientation_change[0].value()-0.5)*math.pi
        delta_pitch = -(self.robot_orientation_change[1].value()-0.5)*math.pi
        delta_yaw = -(self.robot_orientation_change[2].value()-0.5)*math.pi
        delta_euler = I2P.ttypes.Vec3(delta_roll,delta_pitch,delta_yaw)
        orientation_changed = euler_to_quaternion(delta_euler)
        self.tracking_service.objectMovedDelta(sensorID, timestamp, object_name,position_changed,orientation_changed)
        self.robotInf.moved(position_changed)
        if object_name == 'nao_head':
            self.robotInf.head_rotated(orientation_changed)
            self.tracking_service.objectMoved(sensorID, timestamp, object_name, self.robotInf.position,self.robotInf.head_orientation)
        else:
            self.robotInf.body_rotated(orientation_changed)
            self.tracking_service.objectMoved(sensorID, timestamp, object_name, self.robotInf.position,self.robotInf.body_orientation)
                        
# main
if __name__=='__main__':
    window = MainWindow(700, 200, 320, 500)
    window.end()
    window.show(len(sys.argv), sys.argv)
    Fl.run()